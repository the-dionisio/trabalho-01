###  The Dioniso
##### Trabalho [ 1 ] Linguagem de Programação para Dispositivos Móveis

Trabalho para treino de lógica, usando java. 
O projeto está utilizando spring + mvn, quando possível implementamos práticas do java 8

##### Start via maven:

```sh
mvn spring-boot:run
```
##### Gerando jar (opcional):
```sh
mvn package
#o jar é gerado na pasta target 
```
#
____
##### Lista de exerícios
1) João fez uma pesquisa em seu site de busca predileto, e encontrou a resposta que estava procurando no terceiro link listado. Além disso, ele viu, pelo site, que x pessoas já haviam clicado neste link antes. João havia lido anteriormente, também na Internet, que o número de pessoas que clicam no segundo link listado é o dobro de número de pessoas que clicam no terceiro link listado. Nessa leitura, ele também descobriu que o número de pessoas que clicam no segundo link é a metade do número de pessoas que licam no primeiro link. João está intrigado para saber quantas pessoas clicaram no rimeiro link da busca, e, como você é amigo dele, quer sua ajuda nesta tarefa.

>Entrada

Apenas um número, x, que representa o número de pessoas que clicaram no terceiro link da busca.

>Saída

Mostre apenas um inteiro, indicando quantas pessoas clicaram no primeiro link,
nessa busca.

2) Leonardo é um garoto muito criativo. Ele adora criar desafios para seus colegas da escola. Seu último desafio é o seguinte: diversos números são ditos em voz alta, quando o número 0 (zero) é dito então o desafio termina e seus colegas devem dizer imediatamente qual foi o maior número. Leonardo tem muita dificuldade de verificar se a resposta dada pelos colegas é correta ou não, pois a sequência de números costuma ser longa. Por este motivo, ele resolveu pedir sua ajuda. Sua tarefa é escrever um programa que dada uma sequência de números inteiros positivos terminada por 0 (zero), imprime o maior número da sequência.

>Entrada

Uma sequência de números inteiros positivos diferentes de zero, pois o zero representa o término dos números da sequência.

>Saída

Mostrar o maior número dentre os números da entrada.

3) Foi montado um campeonato de futebol com diversos times. A pontuação para identificar o time vitorioso segue as seguintes regras:
* Cada vitória conta três pontos
* Cada empate um ponto.

Fica melhor classificado no campeonato o time que tenha mais pontos. Em caso de empate no número de pontos, fica melhor classificado o time que tiver maior saldo de gols. Se o número de pontos e o saldo de gols forem os mesmos para mais de um time, então eles estão empatados no campeonato.
Dados os números de vitórias e empates, e os saldos de gols dos times, sua tarefa é determinar a classificação final dos times.

>Entrada

Deverá ser informado o número de times participantes. Para cada time informar :
* Número de vitórias
* Número de empates
* Saldo de gols

>Saída

Mostrar a relação de times por ordem decrescente de classificação

4) Foi definido que todas as barras de chocolate são quadradas. Ana Maria tem uma barra quadrada de chocolate de lado L, e quer compartilhar com alguns colegas. Como ela é muito honesta, então, ela divide a barra em quatro pedaços quadrados, de lado L/2. Depois, ela repete esse procedimento com cada pedaço gerado, sucessivamente, enquanto o lado for maior do que, ou igual a 2cm. Você deve escrever um programa que, dado o lado L da barra inicial, em centímetros, determina quantos pedaços haverá ao final do processo.

>Entrada

A entrada em um único inteiro, L, que corresponde ao número de centímetros do lado do quadrado.

>Saída

O programa deve imprimir um único inteiro que representa o número total de pedaços obtidos.

5) Uma quadra de tênis tem o formato de um retângulo, cujos lados medem 432 polegadas por 936 polegadas, tendo 1,97 polegadas a largura das faixas das bordas. Em um Grand Slam na Austrália, Rafael Nadal perdeu para Novak Djokovic, num dos jogos mais bonitos de tênis.Muitas vezes, uma jogada é tão rápida, e a bola tão próxima da borda da quadra, que o juiz pode tomar uma decisão que pode ser contestada por um dos jogadores. Para isso, existe o desafio, que utiliza a imagem gravada do jogo para decidir se a bola estava dentro ou fora da metade da quadra correspondente a um dos jogadores. Considere que a semi-quadra de Rafael Nadal corresponde a um retângulo em que dois vértices têm coordenadas (0,0) e (432, 468), onde todos os números são em polegadas.
Você deve escrever um programa para, dadas as coordenadas (X; Y ) do ponto de ontato da bola com o solo, determinar se uma bola bateu no solo:
* Dentro da semi-quadra
* Fora da semi-quadra.
* Na linha da semi-quadra

>Entrada

A entrada contém dois inteiros X e Y , que correspondem às coordenadas do ponto (X; Y ) de contato da bola com o solo, em polegadas.

>Saída

O programa deve imprimir a palavra:

* dentro se a bola bateu dentro da semi-quadra,
* fora se a bola bateu fora da semi-quadra
* linha se a bola bateu na linha da semi-quadra