package the.dionisio;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import the.dionisio.menu.Menu;

import java.io.IOException;

@SpringBootApplication
public class Main {

	public static void main(String[] args) throws IOException {

		new Menu().start();

	}
}
