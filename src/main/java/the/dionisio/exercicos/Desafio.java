package the.dionisio.exercicos;

import the.dionisio.util.Util;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 4/4/17.
 */
public class Desafio {

    public void start() throws IOException {
        String n1 = Util.input.line("Informa o primeiro numero: ");
        String n2 = Util.input.line("Informe o segundo numero: ");

        List<Integer> l1 = new ArrayList<>();
        List<Integer> l2 = new ArrayList<>();

        List<Integer> r1 = new ArrayList<>();
        List<Integer> r2 = new ArrayList<>();

        for (int i = 0; i< n1.length(); i++)
        {
            l1.add(Integer.parseInt(String.valueOf(n1.charAt(i))));
        }

        for (int i = 0; i< n2.length(); i++)
        {
            l2.add(Integer.parseInt(String.valueOf(n2.charAt(i))));
        }

        l2.forEach(l->{
            multiplica(l1,l);
            System.out.println("");
        });


    }

    private void multiplica(List<Integer> list, Integer f)
    {
        Integer sobra =0;
        List<String> r = new ArrayList<>();
        int cS =0;

        for (int i = list.size(); i > 0; i--)
        {
            if (cS==0)
            {
                cS ++;
            }
            else
            {
                sobra = 0;
            }
            Integer result = list.get(i-1) * f + sobra;
            if (result.toString().length()>1&& i!=0)
            {
                sobra = Integer.parseInt(String.valueOf(result.toString().charAt(0)));
                r.add(String.valueOf(result.toString().charAt(1)));
                cS = 0;
            }

        }

        r.add(String.valueOf(f*list.get(0)+sobra));

        for (int i = r.size(); i>0; i --)
        {
            System.out.printf(r.get(i-1));
        }
    }
}
