package the.dionisio.exercicos;

import the.dionisio.menu.Menu;
import the.dionisio.util.Util;

import java.io.IOException;

/**
 * Created by jonathan on 4/2/17 and atualized by Mateus on 6/4/17.
 */
public class Ex01 {
    public void start() throws IOException {
        Menu.exAtual = 1;
        int link1,link2,link3;

        System.out.println("");
        System.out.println("##########################");
        System.out.println("######EXERCICIO - 01######");
        System.out.println("##########################");
        System.out.println("");

        try
        {
            link3 = Integer.parseInt(Util.input.line("Digite o número de pessoas que clicaram no Terceiro Link: "));
            System.out.println("");
            link2 = (link3*2);
            link1 = (link2*2);

            System.out.println("----------------------------------------");
            System.out.println("Quantidade de Clicks Nos Links :");
            System.out.println("Primeiro link : " + link1);
            System.out.println("Segundo link : " + link2);
            System.out.println("Terceiro link : " + link3);
            System.out.println("----------------------------------------");
            System.out.println("");
        }
        catch(Exception e){
            new Menu().error("Valor digitado inválido.");
        }
        finally{
            new Menu().footer(1);
        }
    }
}
