package the.dionisio.exercicos;
import the.dionisio.menu.Menu;
import the.dionisio.util.Util;

import java.io.IOException;

/**
 * Created by jonathan on 4/2/17.
 */
public class Ex02 {
    public void start() throws IOException {

        Menu.exAtual =2;

        int num = 1;
        boolean erro = false;
        int max = Integer.MIN_VALUE;
        System.out.println("DIGITE [0] PARAR PARAR!");
        while(num != 0)
        {
            try
            {
                num = Integer.parseInt(Util.input.line("Digite número inteiro: "));
                if(num > max)
                {
                    max = num;
                }
            }
            catch(Exception e)
            {
                num = 0;
                erro = true;
            }
        }
        if(erro != true)
        {
            System.out.println("Valor máxmio dessa sequencia : " + max);
        }
        else
        {
            System.out.println("Digite apenas números inteiros!");
        }
            new Menu().footer(2);
    }
}