package the.dionisio.exercicos;

import the.dionisio.dto.Time;
import the.dionisio.menu.Menu;
import the.dionisio.util.Util;

import javax.xml.transform.sax.SAXSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jonathan on 4/2/17.
 */
public class Ex03 {
    public void start() throws IOException {

        Menu.exAtual = 3;

        List<Time> times = new ArrayList<>();

        Integer quantidadeTime = 0;
        quantidadeTime = validation(quantidadeTime, " Digite a quantidade de times participantes: ");

        for (int i= 0; i< quantidadeTime; i++)
        {
            Time time = new Time();
            System.out.println(" INFORMAÇÕES DO " + (i+1) + "º TIME");
            time.quantidadeVitoria = validation(time.quantidadeVitoria," vitorias: ");
            time.quantidadeEmpate = validation(time.quantidadeEmpate," empates: ");
            time.quantidadeGols = validation(time.quantidadeGols," gols: ");
            System.out.println();
            time.totalPontos = ((time.quantidadeVitoria*3) + (time.quantidadeEmpate));
            time.classificacao = Float.parseFloat(time.totalPontos + "." + time.quantidadeGols);
            time.posicao = (i+1) + "º LUGAR";
            times.add(time);
        }

        times.sort((o1, o2) -> o2.classificacao.compareTo(o1.classificacao));

        for (int i= 0; i< quantidadeTime; i++)
        {
            times.get(i).posicao = (i+1) + "º LUGAR";
        }

        times.forEach(t->{
            System.out.println("\n " + t.posicao +
                    "\n Quantidade de pontos: " + t.totalPontos +
                    "\n Quantidade de vitórias: " + t.quantidadeVitoria +
                    "\n Quantidade de empates: " + t.quantidadeEmpate +
                    "\n Quantidade de gols: " + t.quantidadeGols);
        });

        new Menu().footer(3);
    }

    private Integer validation(Integer campo, String mensagem){
        do
        {
            try
            {
                campo = Integer.parseInt(
                        Util.input.line(mensagem));
                break;
            }
            catch (Exception err)
            {
                System.out.println("Favor digitar apenas números inteiros!");
            }
        }while(true);
        return campo;
    }
}
