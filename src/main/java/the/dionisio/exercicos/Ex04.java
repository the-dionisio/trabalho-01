package the.dionisio.exercicos;
import the.dionisio.menu.Menu;

import java.io.IOException;
import java.util.Scanner;
/**
 * Created by jonathan on 4/2/17.
 */
public class Ex04 {
    public void start() throws IOException {
        Menu.exAtual = 4;
        double tamanho;
        int pedaco;
        boolean x = false;
        String valor = "";

        Scanner s = new Scanner(System.in);

        do{
            x = false;
            System.out.println("Digite o tamanho do lado do chocolate\n");
            valor = s.nextLine();
            try{
                tamanho = Double.parseDouble(valor);
                x = false;
                pedaco = 1;
                while(tamanho >= 2){
                    tamanho = tamanho /2;
                    pedaco *= 4;
                }
                System.out.println("\nSão " + pedaco + " pedaços de chocolate\n");
            }catch (Exception e) {
                System.out.println("Por favor digitar um numero\n");
                x = true;
            }
        }while(x);

        new Menu().footer(4);

    }
}
