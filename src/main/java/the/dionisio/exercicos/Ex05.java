package the.dionisio.exercicos;


import the.dionisio.menu.Menu;
import the.dionisio.util.Util;

import javax.sound.midi.Soundbank;
import java.io.IOException;

/**
 * Created by jonathan on 4/2/17.
 */
public class Ex05 {
    public void start() throws IOException {

        Menu.exAtual =5;

        try
        {
            String reponse = Util.input.line(" Informe as coordenadas separadas por virgulas. " +
                                                "\n Exemplo: x,y" +
                                                "\n Informe: ");
            String [] splited = reponse.split(",");

            Integer x = Integer.parseInt(splited[0]);
            Integer y = Integer.parseInt(splited[1]);

            if (x<432 && y<468 && y>-1 && x>-1)
            {
                if (x>430 | y>466)
                {
                    System.out.println(" ➤ Caiu na faixa !");
                }
                else
                {
                    System.out.println(" ➤ Caiu na quadra !");
                }
            }
            else
            {
                System.out.println(" ➤ Caiu fora !");
            }

        }
        catch (Exception e)
        {
            new Menu().error("Erro na converção de integer.");
        }
        finally
        {
            new Menu().footer(5);
        }

    }
}
