package the.dionisio.exercicos;

/**
 * Created by jonathan on 4/2/17.
 */
public interface Exercicio {
    Ex01 ex01 = new Ex01();
    Ex02 ex02 = new Ex02();
    Ex03 ex03 = new Ex03();
    Ex04 ex04 = new Ex04();
    Ex05 ex05 = new Ex05();
    Desafio desf = new Desafio();
}
