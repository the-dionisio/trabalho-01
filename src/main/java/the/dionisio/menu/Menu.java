package the.dionisio.menu;


import the.dionisio.exercicos.Exercicio;
import the.dionisio.util.Util;

import java.io.IOException;

/**
 * Created by jonathan on 4/2/17.
 */
public class Menu{

    public static int exAtual = 0;
    public void start() throws IOException {
        System.out.println("");
        System.out.println("##########################");
        System.out.println("# Grupo: The Dionsio     #");
        System.out.println("# Trabalho-01            #");
        System.out.println("##########################");
        System.out.println("");
        System.out.println("########## MENU ##########");
        System.out.println("#                        #");
        System.out.println("# Exercício 01 ------> 1 #");
        System.out.println("# Exercício 02 ------> 2 #");
        System.out.println("# Exercício 03 ------> 3 #");
        System.out.println("# Exercício 04 ------> 4 #");
        System.out.println("# Exercício 05 ------> 5 #");
        System.out.println("#                        #");
        System.out.println("##########################");
        System.out.println("");

        try
        {
            goTo(Integer
                .parseInt(Util
                          .input.line(" Informe o exercício :")));

        }
        catch (Exception e)
        {
            System.out.println("");
            System.out.println(" ERRO NO PROCESSAMENTO DAS INFORMAÇÕES");
            System.out.println("");
            footer(exAtual);
        }
    }

    public void goTo(Integer exercicio) throws IOException {
        switch (exercicio)
        {
            case 1:
                Exercicio.ex01.start();
                break;
            case 2:
                Exercicio.ex02.start();
                break;
            case 3:
                Exercicio.ex03.start();
                break;
            case 4:
                Exercicio.ex04.start();
                break;
            case 5:
                Exercicio.ex05.start();
                break;
            case 6:
                Exercicio.desf.start();
                break;
            default:
                error(" Isso no ecxist");
                footer(0);

        }

    }

    public void error(String error) throws IOException {
        System.out.println("----------------------------------------");
        System.out.println(" Erro no processamento das informações.");
        System.out.println(" Detalhe: "+error);
        System.out.println("----------------------------------------");
        System.out.println("");
    }

    public void footer(Integer ex) throws IOException {
        String response ="";

        do
        {
           response = Util.input.line(" Escolha: \n    [S]air " +
                                                     "\n    [R]einiciar esse exercício " +
                                                     "\n    [V]oltar ao menu"+
                                                     "\n Sua opcão: ").toLowerCase();
        }
         while(
               !response.equals("s") &
               !response.equals("r") &
               !response.equals("v")
              );

         switch (response)
         {
             case "s":
                 System.out.println("");
                 System.out.println("bye \uD83D\uDC4D");
                 System.out.println("");
                 break;
             case "r":
                 if (exAtual!=0)goTo(ex);
                 start();
                 break;
             case "v":
                 start();
                 break;
         }
    }
}
