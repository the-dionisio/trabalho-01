package the.dionisio.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by jonathan on 4/2/17.
 */
public class Input {

    public String line (String label) throws IOException {
        System.out.print(label);
        return new BufferedReader(new InputStreamReader(System.in)).readLine();
    }
}
